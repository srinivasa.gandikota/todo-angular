import { InMemoryDbService } from 'angular-in-memory-web-api';
import { ToDo } from './todo';

export class InMemoryDataService  implements InMemoryDbService {

  createDb() {
    const todos:  ToDo[] = [
      {
        label: 'Eat pizza',
        id: 0,
        complete: false
      },
      {
        label: 'Do some coding',
        id: 1,
        complete: false
      },
      {
        label: 'Sleep',
        id: 2,
        complete: false
      },
      {
        label: 'Print tickets',
        id: 3,
        complete: false
      }
    ];
    return {todos};
  }


}
