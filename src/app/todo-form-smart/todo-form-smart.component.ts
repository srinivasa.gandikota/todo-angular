import { Component, OnInit } from '@angular/core';

import { ToDoService } from "../todo.service";
import { ToDo } from "../todo";

@Component({
  selector: 'app-todo-form-smart',
  templateUrl: './todo-form-smart.component.html',
  styleUrls: ['./todo-form-smart.component.css']
})
export class TodoFormSmartComponent {

  constructor(private todoService: ToDoService) {
   // this.appTodos = this.todoService.getTodos();
  }

  addTodo(item: { label: string }) {
    this.todoService.addTodo(item);
  }

}
