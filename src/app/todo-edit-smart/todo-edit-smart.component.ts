import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToDoService } from '../todo.service';

@Component({
  selector: 'app-todo-edit-smart',
  templateUrl: './todo-edit-smart.component.html',
  styleUrls: ['./todo-edit-smart.component.css']
})
export class TodoEditSmartComponent implements OnInit {

selectedTodo;
  constructor(private route: ActivatedRoute, private todoService: ToDoService) {}
  ngOnInit() {
    this.route.params.subscribe((params) => {
     const todo =  this.todoService.findById(parseInt(params.id, 10))
     .then(x => (this.selectedTodo = x));
      //this.selectedTodo = todo;
    });
  }

  updateToDo(todo) {
    this.todoService.update(todo);
  }

}
