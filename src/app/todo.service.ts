import { Injectable } from "@angular/core";
import { ToDo } from "./todo";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ToDoService {

  constructor(private http: HttpClient) {}


  getTodos() {
    return this.http.get<ToDo[]>('api/todos').toPromise();
  }

  addTodo(item: { label: string }) {
    return  this.http.post<ToDo>('api/todos', {
        label: item.label,
        complete: false
      }).toPromise();
  }

  completeToDo(todo: ToDo) {
   return this.http.put<ToDo>(`api/todos`, {...todo, complete: true}).toPromise();
    // this.todos = this.todos.map(
    //   item => (item.id === todo.id ? { ...item, complete: true } : item)
    // );
  }

  removeTodo(todo: ToDo) {
    return this.http.delete<ToDo>(`api/todos/${todo.id}`).toPromise();

    // this.todos = this.todos.filter(it => it.id !== todo.id);
  }

  findById(id: number) {
   return  this.http.get<ToDo>(`api/todos/${id}`).toPromise();
  }

  update(todo) {
    this.http.put<ToDo>(`api/todos`, todo).toPromise();

    // this.todos = [...this.todos.filter(it => it.id !== todo.id), todo];
  }
}
