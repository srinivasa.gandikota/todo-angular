import { Component, OnInit } from "@angular/core";
import { ToDoService } from "../todo.service";
import { ToDo } from "../todo";
import { templateJitUrl } from "@angular/compiler";

@Component({
  selector: "app-todos",
  templateUrl: "./todos.component.html",
  styleUrls: ["./todos.component.css"]
})
export class TodosComponent {
  appTodos: ToDo[];
  constructor(private todoService: ToDoService) {
    this.getAllTodos();
  }

  // onAllTodosFromServer(alltodos) {
  //   this.appTodos = alltodos;
  // }

  getAllTodos() {
    this.todoService.getTodos().then(x => (this.appTodos = x));
  }

  addTodo(item: { label: string }) {
    this.todoService.addTodo(item);
  }
  completeTodo(item: { todo: ToDo }) {
    this.todoService.completeToDo(item.todo).then(x => this.getAllTodos());
    // this.appTodos = this.todoService.getTodos();
  }

  // Object.assign({}, item, {complete: true})

  removeTodo(item: { todo: ToDo }) {
    this.todoService.removeTodo(item.todo).then(x => this.getAllTodos());
    // this.appTodos = this.todoService.getTodos();
  }
}
