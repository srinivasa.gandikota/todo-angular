import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ToDoService } from './todo.service';
import { TodoComponent } from './todo/todo.component';
import { TodolistComponent } from './todolist/todolist.component';
import { TodosComponent } from './todos/todos.component';
import { TodoFormComponent } from './todo-form/todo-form.component';
// import { TodoFormSmartComponent } from './todo-form/todo-form-smart.component';

import { Routes, RouterModule } from '@angular/router';
import { TodoFormSmartComponent } from './todo-form-smart/todo-form-smart.component';
import { TodoEditComponent } from './todo-edit/todo-edit.component';
import { TodoEditSmartComponent } from './todo-edit-smart/todo-edit-smart.component';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { HttpClientModule } from '@angular/common/http';

export const ROUTES: Routes = [{
  path: 'todos', component: TodosComponent
}, {
  path: 'todo/new', component: TodoFormSmartComponent
},
{path: 'todos/:id/edit', component: TodoEditSmartComponent}];


@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodolistComponent,
    TodosComponent,
    TodoFormComponent,
    TodoFormSmartComponent,
    TodoEditComponent,
    TodoEditSmartComponent
    // TodoFormSmartComponent
  ],
  imports: [
    BrowserModule, FormsModule, RouterModule.forRoot(ROUTES),
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [ToDoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
